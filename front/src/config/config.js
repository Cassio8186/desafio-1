module.exports = {
	baseUrl:
		process.env.NODE_ENV == "production"
			? "https://stefanini-test-cassio.herokuapp.com/api/v1"
			: "http://localhost:8180/api/v1"
};
