const axios = require("axios").default;
const { baseUrl } = require("../../config/config");

const getListaTarefas = async () => {
	try {
		const response = await axios.get(baseUrl + "/lista-tarefa");
		return response;
	} catch (error) {
		return error;
	}
};

const getListaTarefa = async ({ id }) => {
	try {
		const response = await axios.get(baseUrl + "/lista-tarefa/" + id);
		return response;
	} catch (error) {
		return error;
	}
};

const saveListaTarefa = async ({ nome }) => {
	try {
		const response = await axios.post(baseUrl + "/lista-tarefa/", { nome });
		return response;
	} catch (error) {
		return error;
	}
};

const updateListaTarefa = async ({ id, nome }) => {
	try {
		const response = await axios.put(baseUrl + "/lista-tarefa/", { id, nome });
		return response;
	} catch (error) {
		return error;
	}
};

const deleteListaTarefa = async ({ id }) => {
	try {
		const response = await axios.delete(baseUrl + "/lista-tarefa/" + id);
		return response;
	} catch (error) {
		return error;
	}
};

const deleteAllListasTarefas = async () => {
	try {
		const response = await axios.delete(baseUrl + "/lista-tarefa/");
		return response;
	} catch (error) {
		return error;
	}
};

module.exports = {
	getListaTarefas,
	getListaTarefa,
	saveListaTarefa,
	updateListaTarefa,
	deleteListaTarefa,
	deleteAllListasTarefas
};
