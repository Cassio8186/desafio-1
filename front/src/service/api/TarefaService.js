const axios = require("axios").default;
const { baseUrl } = require("../../config/config");

const getTarefas = async ({ idListaTarefa }) => {
	try {
		const response = await axios.get(
			`${baseUrl}/lista-tarefa/${idListaTarefa}/tarefas`
		);
		return response;
	} catch (error) {
		return error;
	}
};
const saveTarefa = async ({ idListaTarefa, descricao }) => {
	try {
		console.log(idListaTarefa, descricao);
		const response = await axios.post(`${baseUrl}/tarefa`, {
			idListaTarefa,
			descricao
		});

		return response;
	} catch (error) {
		return error;
	}
};
const deleteTarefa = async ({ id }) => {
	try {
		const response = await axios.delete(`${baseUrl}/tarefa/${id}`);

		return response;
	} catch (error) {
		return error;
	}
};

module.exports = {
	getTarefas,
	deleteTarefa,
	saveTarefa
};
