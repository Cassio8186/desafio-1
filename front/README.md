# front

## Instalação dos pacotes do projeto

```bash
yarn install
```

### Para rodar localmente

#### Front

```bash
yarn dev
```

#### Back

```bash
yarn dev:back
```

### Rodar testes unitários

```bash
yarn test:unit
```

### Verificação erros e formatação de código

```bash
yarn lint
```
