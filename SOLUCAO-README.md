# Solução

## Backend

### Tecnologias utilizadas

- Spring Boot
- Spring JPA
- MapStruct
- Lombok
- Junit 5
- Swagger(Documentação API)

### Requisitos

- [x] Usuário pode inserir uma tarefa em uma lista
- [x] Usuário pode alterar uma tarefa de uma lista
- [x] Usuário pode deletar uma tarefa de uma lista
- [x] Usuário pode ter mais de uma lista
- [x] Usuário pode Adicionar listas de tarefas
- [x] Usuário pode Alterar listas de tarefas
- [x] Usuário pode Remover listas de tarefas

### Heroku

Solução hospedada no heroku:  
<https://stefanini-test-cassio.herokuapp.com/>

## FrontEnd

### Tecnologias Utilizadas

- VueJS
- VueBootstrap
- Docker

### Requisitos

- [x] Usuário pode inserir uma tarefa em uma lista
- [x] Usuário pode deletar uma tarefa de uma lista
- [x] Usuário pode remover uma tarefa de uma lista
- [x] Usuário pode ter mais de uma lista
- [x] Usuário pode Adicionar listas de tarefas
- [x] Usuário pode Remover listas de tarefas

### Heroku

<https://stefanini-test-front-cassio.herokuapp.com/>
