package br.com.stefanini.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.dto.ListaTarefaDTO;
import br.com.stefanini.api.model.dto.ListaTarefaSaveDTO;
import br.com.stefanini.api.model.dto.ListaTarefaUpdateDTO;
import br.com.stefanini.api.model.dto.mapper.ListaTarefaMapper;
import br.com.stefanini.api.model.dto.mapper.ListaTarefaSaveMapper;
import br.com.stefanini.api.model.dto.mapper.ListaTarefaUpdateMapper;
import br.com.stefanini.api.service.ListaTarefaService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "api/v1/lista-tarefa", produces = MediaType.APPLICATION_JSON_VALUE)
public class ListaTarefaController {

	@Autowired
	private ListaTarefaService listaTarefaService;

	@GetMapping
	@ApiOperation(value = "Retorna uma lista com todas as listas de tarefas")
	@CrossOrigin
	public ResponseEntity<List<ListaTarefaDTO>> findAllListasTarefas() {
		log.info("Retornando uma lista de tarefas");
		List<ListaTarefa> listasTarefas = this.listaTarefaService.findAll();
		List<ListaTarefaDTO> listasTarefasDTO = ListaTarefaMapper.INSTANCE.toDto(listasTarefas);

		return ResponseEntity.ok(listasTarefasDTO);
	}

	@ApiOperation(value = "Retorna uma lista de tarefas por id")
	@GetMapping(value = "/{id}")
	@CrossOrigin
	public ResponseEntity<ListaTarefaDTO> findAllListaTarefaById(@PathVariable("id") Long id) {
		log.info("Retornando uma lista de tarefas");
		ListaTarefa listaTarefa = this.listaTarefaService.findById(id);
		ListaTarefaDTO listasTarefasDTO = ListaTarefaMapper.INSTANCE.toDto(listaTarefa);

		return ResponseEntity.ok(listasTarefasDTO);
	}

	@ApiOperation(value = "Salva uma lista de tarefas")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<ListaTarefaDTO> saveListaTarefas(@RequestBody ListaTarefaSaveDTO listaTarefaSaveDTO) {
		log.info("Salvando uma lista de tarefas");
		ListaTarefa listaTarefa = ListaTarefaSaveMapper.INSTANCE.toEntity(listaTarefaSaveDTO);

		ListaTarefa listaSalva = this.listaTarefaService.save(listaTarefa);

		ListaTarefaDTO listasTarefasDTO = ListaTarefaMapper.INSTANCE.toDto(listaSalva);

		return ResponseEntity.ok(listasTarefasDTO);
	}

	@ApiOperation(value = "Altera uma lista de tarefas por id")
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<ListaTarefaDTO> updateListaTarefas(@RequestBody ListaTarefaUpdateDTO listaTarefaUpdateDTO) {
		log.info("Alterando uma lista de tarefas");
		ListaTarefa listaTarefa = ListaTarefaUpdateMapper.INSTANCE.toEntity(listaTarefaUpdateDTO);

		this.listaTarefaService.throwErrorIfNotExistsById(listaTarefa.getId());

		ListaTarefa listaAlterada = this.listaTarefaService.update(listaTarefa);

		ListaTarefaDTO listasTarefasDTO = ListaTarefaMapper.INSTANCE.toDto(listaAlterada);

		return ResponseEntity.ok(listasTarefasDTO);
	}

	@ApiOperation(value = "Deleta uma lista de tarefas por id")
	@DeleteMapping(value = "/{id}")
	@CrossOrigin
	@ResponseStatus(code = HttpStatus.OK, value = HttpStatus.OK)
	public ResponseEntity<?> deleteListaTarefaById(@PathVariable("id") Long id) {
		log.info("Removendo uma lista de tarefas");

		this.listaTarefaService.throwErrorIfNotExistsById(id);

		this.listaTarefaService.delete(id);

		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Deleta todas as listas de tarefas")
	@DeleteMapping
	@ResponseStatus(code = HttpStatus.OK, value = HttpStatus.OK)
	@CrossOrigin
	public ResponseEntity<?> deleteAllListaTarefas() {
		log.info("Removendo todas as listas de tarefa");
		this.listaTarefaService.deleteAll();

		return ResponseEntity.noContent().build();
	}
}
