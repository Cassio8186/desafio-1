package br.com.stefanini.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "lista_tarefa")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListaTarefa implements Serializable {

	private static final long serialVersionUID = 8404670026963278524L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String nome;

	@OneToMany(mappedBy = "listaTarefa", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE })
	private List<Tarefa> tarefas = new ArrayList<>();

	@Override
	public String toString() {
		return "ListaTarefa [id=" + id + ", nome=" + nome + ", tarefas=" + tarefas + "]";
	}

}
