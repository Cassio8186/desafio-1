package br.com.stefanini.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.repository.ListaTarefaRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class ListaTarefaService {

	private static final String TAREFA_ENTITY = "listaTarefa";

	@Autowired
	private ListaTarefaRepository listaTarefaRepository;

	public void delete(Long idListaTarefa) {
		throwErrorIfNotExistsById(idListaTarefa);

		this.listaTarefaRepository.deleteById(idListaTarefa);

		log.info(String.format("Entidade %s de id: %d removida do banco de dados", TAREFA_ENTITY, idListaTarefa));
	}

	public List<ListaTarefa> findAll() {
		log.info("Retornando todas listas de tarefas encontradas");
		return this.listaTarefaRepository.findAll();
	}

	public ListaTarefa findById(Long id) {
		Optional<ListaTarefa> optionalListaTarefa = this.listaTarefaRepository.findById(id);

		if (!optionalListaTarefa.isPresent()) {
			throw new EntityNotFoundException(ListaTarefa.class, "id", id);
		}

		ListaTarefa listaTarefa = optionalListaTarefa.get();

		log.info(String.format("findById: Entidade %s encontrada pela cláusula[%s: %d]", TAREFA_ENTITY, "id", id));
		return listaTarefa;

	}

	public ListaTarefa save(ListaTarefa listaTarefa) {
		ListaTarefa listaTarefaSalvo = this.listaTarefaRepository.save(listaTarefa);
		log.info(String.format("Entidade %s inserido no banco de dados, %s", TAREFA_ENTITY,
				listaTarefaSalvo.toString()));
		return listaTarefaSalvo;
	}

	public void throwErrorIfNotExistsById(Long id) {
		Boolean listaExists = this.listaTarefaRepository.existsById(id);
		if (!listaExists) {
			throw new EntityNotFoundException(ListaTarefa.class, "id", id);
		}

	}

	public ListaTarefa update(ListaTarefa listaTarefa) {
		throwErrorIfNotExistsById(listaTarefa.getId());

		ListaTarefa listaTarefaAtualizado = this.listaTarefaRepository.save(listaTarefa);
		log.info(String.format("Entidade %s de id: %d atualizada no banco de dados, %s", TAREFA_ENTITY,
				listaTarefa.getId(),
				listaTarefaAtualizado.toString()));
		return listaTarefaAtualizado;
	}

	public void deleteAll() {
		log.info("Removendo todas listas de tarefas");
		this.listaTarefaRepository.deleteAll();
	}
}
