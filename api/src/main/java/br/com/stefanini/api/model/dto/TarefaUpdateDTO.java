package br.com.stefanini.api.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TarefaUpdateDTO {
	@ApiModelProperty(value = "id da tarefa que será atualizada", required = true)
	private Long id;

	@ApiModelProperty(name = "Descricao da tarefa", example = "Comprar pão", required = true)
	@NotNull
	@NotBlank
	private String descricao;

	@Override
	public String toString() {
		return "TarefaUpdateDTO [id=" + id + ", descricao=" + descricao + "]";
	}
}
