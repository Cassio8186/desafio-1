package br.com.stefanini.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.stefanini.api.model.Tarefa;

@Repository
public interface TarefaRepository extends JpaRepository<Tarefa, Long> {
	List<Tarefa> findAllByListaTarefaId(Long id);

	Boolean existsByListaTarefaIdAndDescricao(Long listaTarefaId, String descricao);
}
