package br.com.stefanini.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListaTarefaDTO {

	private Long id;

	@ApiModelProperty(value = "Nome da Lista de tarefas", example = "Deveres de casa")
	private String nome;

	@Override
	public String toString() {
		return "ListaTarefaDTO [id=" + id + ", nome=" + nome + "]";
	}

}
