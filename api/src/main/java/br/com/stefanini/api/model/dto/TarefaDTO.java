package br.com.stefanini.api.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TarefaDTO {

	private Long id;

	@ApiModelProperty(name = "Descricao da tarefa", example = "Comprar pão")
	private String descricao;

	@ApiModelProperty(name = "Id da lista de tarefas")
	private Long idListaTarefa;

	@Override
	public String toString() {
		return "TarefaDTO [id=" + id + ", descricao=" + descricao + ", idListaTarefa=" + idListaTarefa + "]";
	}
}
