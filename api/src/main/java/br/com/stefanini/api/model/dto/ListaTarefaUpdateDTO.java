package br.com.stefanini.api.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListaTarefaUpdateDTO {

	@ApiModelProperty(required = true)
	@NotNull
	private Long id;

	@ApiModelProperty(value = "Nome da Lista de tarefas", example = "Deveres de casa", required = true)
	@NotNull
	@NotBlank
	private String nome;

	@Override
	public String toString() {
		return "ListaTarefaUpdateDTO [id=" + id + ", nome=" + nome + "]";
	}

}
