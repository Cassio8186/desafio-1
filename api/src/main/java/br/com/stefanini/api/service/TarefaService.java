package br.com.stefanini.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.stefanini.api.exceptions.EntityAlreadyExistsException;
import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.Tarefa;
import br.com.stefanini.api.repository.TarefaRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class TarefaService {

	private static final String TAREFA_ENTITY = "tarefa";

	@Autowired
	private TarefaRepository tarefaRepository;

	@Autowired
	private ListaTarefaService listaTarefaService;

	public void delete(Long idTarefa) {
		throwErrorIfNotExistsById(idTarefa);

		this.tarefaRepository.deleteById(idTarefa);

		log.info(String.format("Entidade %s de id: %d removida do banco de dados", TAREFA_ENTITY, idTarefa));
	}

	public List<Tarefa> findAll() {
		log.info("Retornando todas tarefas encontradas");
		return this.tarefaRepository.findAll();
	}

	public List<Tarefa> findAllByListaTarefaId(Long id) {
		listaTarefaService.throwErrorIfNotExistsById(id);

		log.info(String.format("Retornando todas tarefas encontradas por id: %d", id));
		return this.tarefaRepository.findAllByListaTarefaId(id);

	}

	public Tarefa findById(Long id) {
		Optional<Tarefa> optionalTarefa = this.tarefaRepository.findById(id);

		if (!optionalTarefa.isPresent()) {
			throw new EntityNotFoundException(Tarefa.class, "id", id);
		}

		Tarefa tarefa = optionalTarefa.get();

		log.info(String.format("findById: Entidade %s encontrada pela cláusula[%s: %d]", TAREFA_ENTITY, "id", id));
		return tarefa;

	}

	public Tarefa save(Tarefa tarefa) {
		throwErrorIfExistsByTarefaIdAndDescricao(tarefa.getListaTarefa().getId(), tarefa.getDescricao());

		Tarefa tarefaSalvo = this.tarefaRepository.save(tarefa);
		String logMessage = String.format("Entidade %s inserido no banco de dados, %s", TAREFA_ENTITY,
				tarefaSalvo.getDescricao());
		log.info(logMessage);
		return tarefaSalvo;
	}

	public void throwErrorIfNotExistsById(Long id) {
		Boolean tarefaExists = this.tarefaRepository.existsById(id);
		if (!tarefaExists) {
			throw new EntityNotFoundException(Tarefa.class, "id", id);
		}

	}

	private void throwErrorIfExistsByTarefaIdAndDescricao(Long listaTarefaId, String tarefaDescricao) {
		this.listaTarefaService.throwErrorIfNotExistsById(listaTarefaId);

		Boolean tarefaExists = this.tarefaRepository.existsByListaTarefaIdAndDescricao(listaTarefaId,
				tarefaDescricao);
		if (tarefaExists) {
			throw new EntityAlreadyExistsException(Tarefa.class, "descricao", tarefaDescricao);
		}
	}

	public Tarefa update(Tarefa tarefa) {

		Tarefa tarefaSalva = this.findById(tarefa.getId());

		throwErrorIfExistsByTarefaIdAndDescricao(tarefaSalva.getListaTarefa().getId(), tarefa.getDescricao());

		tarefaSalva.setDescricao(tarefa.getDescricao());

		Tarefa tarefaAtualizado = this.tarefaRepository.save(tarefaSalva);
		log.info(String.format("Entidade %s de id: %d atualizada no banco de dados, %s", TAREFA_ENTITY,
				tarefaSalva.getId(),
				tarefaSalva.getDescricao()));
		return tarefaAtualizado;
	}

}
