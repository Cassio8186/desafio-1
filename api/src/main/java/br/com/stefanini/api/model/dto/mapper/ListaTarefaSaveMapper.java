package br.com.stefanini.api.model.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.dto.ListaTarefaSaveDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface ListaTarefaSaveMapper extends EntityMapper<ListaTarefa, ListaTarefaSaveDTO> {
	ListaTarefaSaveMapper INSTANCE = Mappers.getMapper(ListaTarefaSaveMapper.class);

}
