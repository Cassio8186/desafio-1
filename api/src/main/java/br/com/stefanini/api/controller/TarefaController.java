package br.com.stefanini.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.api.model.Tarefa;
import br.com.stefanini.api.model.dto.TarefaDTO;
import br.com.stefanini.api.model.dto.TarefaSaveDTO;
import br.com.stefanini.api.model.dto.TarefaUpdateDTO;
import br.com.stefanini.api.model.dto.mapper.TarefaMapper;
import br.com.stefanini.api.model.dto.mapper.TarefaSaveMapper;
import br.com.stefanini.api.model.dto.mapper.TarefaUpdateMapper;
import br.com.stefanini.api.service.TarefaService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "api/v1/", produces = MediaType.APPLICATION_JSON_VALUE)
public class TarefaController {

	@Autowired
	private TarefaService tarefaService;

	@GetMapping("lista-tarefa/{id-tarefa}/tarefas")
	@ApiOperation(value = "Retorna todas tarefas de uma lista de tarefas")
	@CrossOrigin
	public ResponseEntity<List<TarefaDTO>> findAllTarefasByListaTarefaId(@PathVariable("id-tarefa") Long idTarefa) {
		log.info("Retornando uma lista de tarefas");
		List<Tarefa> tarefas = this.tarefaService.findAllByListaTarefaId(idTarefa);
		List<TarefaDTO> tarefasDTO = TarefaMapper.INSTANCE.toDto(tarefas);

		return ResponseEntity.ok(tarefasDTO);
	}

	@ApiOperation(value = "Retorna uma tarefa por id")
	@GetMapping(value = "tarefa/{id}")
	@CrossOrigin
	public ResponseEntity<TarefaDTO> findByTarefaId(@PathVariable("id") Long id) {
		log.info("Retornando tarefa de id: " + id);

		Tarefa tarefa = this.tarefaService.findById(id);
		TarefaDTO tarefasDTO = TarefaMapper.INSTANCE.toDto(tarefa);

		return ResponseEntity.ok(tarefasDTO);
	}

	@ApiOperation(value = "Salva uma tarefa em uma lista")
	@PostMapping(value = "tarefa", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<TarefaDTO> saveTarefas(@RequestBody TarefaSaveDTO tarefaSaveDTO) {
		log.info("Salvando uma lista de tarefas");
		Tarefa tarefa = TarefaSaveMapper.INSTANCE.toEntity(tarefaSaveDTO);

		Tarefa listaSalva = this.tarefaService.save(tarefa);

		TarefaDTO tarefasDTO = TarefaMapper.INSTANCE.toDto(listaSalva);

		return ResponseEntity.ok(tarefasDTO);
	}

	@ApiOperation(value = "Altera uma tarefa de uma lista")
	@PutMapping(value = "tarefa", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<TarefaDTO> updateTarefas(@RequestBody TarefaUpdateDTO tarefaUpdateDTO) {
		log.info("Alterando uma lista de tarefas");
		Tarefa tarefa = TarefaUpdateMapper.INSTANCE.toEntity(tarefaUpdateDTO);

		Tarefa tarefaAlterada = this.tarefaService.update(tarefa);

		TarefaDTO tarefasDTO = TarefaMapper.INSTANCE.toDto(tarefaAlterada);

		return ResponseEntity.ok(tarefasDTO);
	}

	@ApiOperation(value = "Deleta uma tarefas por id")
	@DeleteMapping(value = "tarefa/{id}")
	@ResponseStatus(code = HttpStatus.OK, value = HttpStatus.OK)
	@CrossOrigin
	public ResponseEntity<?> deleteTarefaById(@PathVariable("id") Long id) {
		log.info("Removendo un"
				+ "ma lista de tarefas");

		this.tarefaService.throwErrorIfNotExistsById(id);

		this.tarefaService.delete(id);

		return ResponseEntity.noContent().build();
	}

}
