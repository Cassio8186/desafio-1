package br.com.stefanini.api.model.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ErrorDTO implements Serializable {

	private static final long serialVersionUID = 8911978209490691030L;

	@NotNull
	@ApiModelProperty("Exceção Java lançada.")
	private String exception;

	@NotNull
	@ApiModelProperty("Mensagem de erro.")
	private String message;

	@NotNull
	@ApiModelProperty("Código HTTP")
	private int code;

	@NotNull
	@ApiModelProperty("Lista com todos erros retornados")
	private List<String> errors;

	@Override
	public String toString() {
		return "ErrorDTO [exception=" + exception + ", message=" + message + ", code=" + code + ", errors=" + errors
				+ "]";
	}

}
