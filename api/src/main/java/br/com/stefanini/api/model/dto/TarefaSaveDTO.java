package br.com.stefanini.api.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TarefaSaveDTO {

	@ApiModelProperty(name = "Descricao da tarefa", example = "Comprar pão", required = true)
	@NotNull
	@NotBlank
	private String descricao;

	@ApiModelProperty(name = "Id da lista de tarefas", required = true)
	@NotNull
	private Long idListaTarefa;

	@Override
	public String toString() {
		return "TarefaSaveDTO [descricao=" + descricao + ", idListaTarefa=" + idListaTarefa + "]";
	}
}
