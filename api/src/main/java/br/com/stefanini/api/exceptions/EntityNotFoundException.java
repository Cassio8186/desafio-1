package br.com.stefanini.api.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -6093025222480347479L;

	@SuppressWarnings("rawtypes")
	public EntityNotFoundException(Class entityClass, String field, Object value) {
		super(String.format("Entidade %s: não encontrada pela cláusula: [%s:%s]",
				entityClass.getSimpleName(), field, value));

		log.error(String.format("Entidade %s: não encontrada pela cláusula: [%s:%s]",
				entityClass.getSimpleName(), field, value));
	}
}
