package br.com.stefanini.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tarefa")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tarefa implements Serializable {

	private static final long serialVersionUID = 8404670026963278524L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String descricao;

	@ManyToOne()
	@JoinColumn(name = "id_lista_tarefa", foreignKey = @ForeignKey(name = "fk_lista_tarefa_id"), nullable = false)
	private ListaTarefa listaTarefa;

	@Override
	public String toString() {
		return "Tarefa [id=" + id + ", descricao=" + descricao + ", listaTarefa=" + listaTarefa + "]";
	}

}
