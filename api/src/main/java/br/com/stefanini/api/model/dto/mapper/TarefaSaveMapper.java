package br.com.stefanini.api.model.dto.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.stefanini.api.model.Tarefa;
import br.com.stefanini.api.model.dto.TarefaSaveDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = { ListaTarefaMapper.class })
public interface TarefaSaveMapper extends EntityMapper<Tarefa, TarefaSaveDTO> {
	TarefaSaveMapper INSTANCE = Mappers.getMapper(TarefaSaveMapper.class);

	@Mapping(source = "listaTarefa.id", target = "idListaTarefa")
	TarefaSaveDTO toDto(Tarefa entity);

	@InheritInverseConfiguration
	Tarefa toEntity(TarefaSaveDTO dto);
}
