package br.com.stefanini.api.model.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.dto.ListaTarefaDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface ListaTarefaMapper extends EntityMapper<ListaTarefa, ListaTarefaDTO> {
	ListaTarefaMapper INSTANCE = Mappers.getMapper(ListaTarefaMapper.class);

};