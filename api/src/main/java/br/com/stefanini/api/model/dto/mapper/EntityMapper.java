package br.com.stefanini.api.model.dto.mapper;

import java.util.List;

/**
 *
 * @param <E> Entidade
 * @param <D> DTO
 */
public interface EntityMapper<E, D> {

	public D toDto(E entity);

	public List<D> toDto(List<E> entity);

	public E toEntity(D dto);

	public List<E> toEntity(List<D> dto);

}
