package br.com.stefanini.api.model.dto.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.stefanini.api.model.Tarefa;
import br.com.stefanini.api.model.dto.TarefaDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = { ListaTarefaMapper.class })
public interface TarefaMapper extends EntityMapper<Tarefa, TarefaDTO> {
	TarefaMapper INSTANCE = Mappers.getMapper(TarefaMapper.class);

	@Mapping(source = "listaTarefa.id", target = "idListaTarefa")
	TarefaDTO toDto(Tarefa entity);

	@InheritInverseConfiguration
	Tarefa toEntity(TarefaDTO dto);

}
