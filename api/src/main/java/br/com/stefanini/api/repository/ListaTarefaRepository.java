package br.com.stefanini.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.stefanini.api.model.ListaTarefa;

@Repository
public interface ListaTarefaRepository extends JpaRepository<ListaTarefa, Long> {

}
