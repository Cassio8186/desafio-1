package br.com.stefanini.api.model.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.stefanini.api.model.Tarefa;
import br.com.stefanini.api.model.dto.TarefaUpdateDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface TarefaUpdateMapper extends EntityMapper<Tarefa, TarefaUpdateDTO> {
	TarefaUpdateMapper INSTANCE = Mappers.getMapper(TarefaUpdateMapper.class);

}
