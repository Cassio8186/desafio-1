package br.com.stefanini.api.model.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.dto.ListaTarefaUpdateDTO;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = {})
public interface ListaTarefaUpdateMapper extends EntityMapper<ListaTarefa, ListaTarefaUpdateDTO> {
	ListaTarefaUpdateMapper INSTANCE = Mappers.getMapper(ListaTarefaUpdateMapper.class);

}
