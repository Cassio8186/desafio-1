package br.com.stefanini.api.controller.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.stefanini.api.exceptions.EntityAlreadyExistsException;
import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.dto.ErrorDTO;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EntityAlreadyExistsException.class)
	public ResponseEntity<ErrorDTO> handleEntityNotFoundException(EntityAlreadyExistsException ex, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(Arrays.asList(ex.getMessage()))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<ErrorDTO> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(Arrays.asList(ex.getMessage()))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ErrorDTO> handleException(RuntimeException ex, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(Arrays.asList(ex.getMessage()))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), httpStatus);

	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		List<String> errors = new ArrayList<>();

		for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": "
					+ violation.getMessage());
		}
		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(errors)
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), httpStatus);
	}

	@Override
	public ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

		StringBuilder builder = new StringBuilder();
		builder.append(ex.getMethod());
		builder.append("Método não suportado para esta requisição. Métodos suportados são:");
		ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())

				.errors(Arrays.asList(builder.toString()))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), httpStatus);
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

		List<String> errors = new ArrayList<>();
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(errors)
				.build();

		return handleExceptionInternal(ex, errorDTO, headers, httpStatus, request);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
			WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

		String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();

		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(Arrays.asList(error))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), httpStatus);
	}

	@Override
	public ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		String error = ex.getParameterName() + " parameter is missing";

		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(Arrays.asList(error))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), httpStatus);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<Object> pSQLExceptionAdvice(DataIntegrityViolationException ex, WebRequest request) {
		final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

		ErrorDTO errorDTO = ErrorDTO.builder()
				.exception(ex.getClass().getSimpleName())
				.message(ex.getLocalizedMessage())
				.code(httpStatus.value())
				.errors(Arrays.asList(ex.getMessage()))
				.build();

		return new ResponseEntity<>(errorDTO, new HttpHeaders(), httpStatus);
	}
}
