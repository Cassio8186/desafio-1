package br.com.stefanini.api.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EntityAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = -6093025222480347479L;

	@SuppressWarnings("rawtypes")
	public EntityAlreadyExistsException(Class entityClass, String field, Object value) {
		super(String.format("Entidade %s: já existe, encontrada pela cláusula: [%s:%s]",
				entityClass.getSimpleName(), field, value));

		log.error(String.format("Entidade %s: já existe, encontrada pela cláusula: [%s:%s]",
				entityClass.getSimpleName(), field, value));
	}
}
