package br.com.stefanini.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.ListaTarefa;

@SpringBootTest
@ActiveProfiles("dev-h2")
class ListaTarefaServiceTest {

	@Autowired
	private ListaTarefaService listaTarefaService;

	private ListaTarefa listaTarefa;
	private ListaTarefa listaTarefa2;

	@BeforeEach
	void setUp() throws Exception {
		listaTarefa = new ListaTarefa();
		listaTarefa2 = new ListaTarefa();

		listaTarefa.setNome("HomeWork");
		listaTarefa2.setNome("Escrever um poema");

	}

	@AfterEach
	void tearDown() throws Exception {
		this.listaTarefaService.deleteAll();
	}

	@Test
	void testDelete() {
		ListaTarefa listaTarefaSalvo = this.listaTarefaService.save(listaTarefa);

		this.listaTarefaService.delete(listaTarefaSalvo.getId());

		assertThrows(EntityNotFoundException.class, () -> {
			this.listaTarefaService.throwErrorIfNotExistsById(listaTarefaSalvo.getId());
		});

	}

	@Test
	void testFindAll() {
		this.listaTarefaService.save(listaTarefa);
		this.listaTarefaService.save(listaTarefa2);

		List<ListaTarefa> listaTarefas = this.listaTarefaService.findAll();

		assertEquals(2, listaTarefas.size());
		assertEquals(listaTarefa.getNome(), listaTarefas.get(0).getNome());

	}

	@Test
	void testFindById() {
		ListaTarefa savedLista = this.listaTarefaService.save(listaTarefa);

		Long expectedId = savedLista.getId();

		ListaTarefa actualUser = this.listaTarefaService.findById(expectedId);

		assertEquals(savedLista.getNome(), actualUser.getNome());

	}

	@Test
	void testSave() {
		ListaTarefa listaTarefaSalvo = this.listaTarefaService.save(listaTarefa);

		assertEquals(listaTarefaSalvo.getNome(), listaTarefa.getNome());

	}

	@Test
	void testThrowErrorIfNotExistsById() {
		assertThrows(EntityNotFoundException.class, () -> {
			this.listaTarefaService.throwErrorIfNotExistsById(200L);
		});
	}

	@Test
	void testUpdate() {
		ListaTarefa listaTarefaAlterar = this.listaTarefaService.save(listaTarefa);
		listaTarefaAlterar.setNome("Novo nome");

		ListaTarefa listaTarefaAlterado = this.listaTarefaService.update(listaTarefaAlterar);

		assertEquals(listaTarefaAlterar.getNome(), listaTarefaAlterado.getNome());

	}

}
