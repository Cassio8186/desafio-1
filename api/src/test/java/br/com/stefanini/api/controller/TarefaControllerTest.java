package br.com.stefanini.api.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.Tarefa;
import br.com.stefanini.api.model.dto.TarefaSaveDTO;
import br.com.stefanini.api.model.dto.TarefaUpdateDTO;
import br.com.stefanini.api.model.dto.mapper.TarefaSaveMapper;
import br.com.stefanini.api.model.dto.mapper.TarefaUpdateMapper;
import br.com.stefanini.api.service.ListaTarefaService;
import br.com.stefanini.api.service.TarefaService;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("dev-h2")
class TarefaControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ListaTarefaService listaTarefaService;

	@Autowired
	private TarefaService tarefaService;

	private ListaTarefa listaTarefaSalva;

	@BeforeEach
	public void setUp() {
		ListaTarefa listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha lista");

		this.listaTarefaSalva = this.listaTarefaService.save(listaTarefa);
	}

	@AfterEach
	public void tearDown() {
		this.listaTarefaService.deleteAll();
		this.listaTarefaSalva = null;
	}

	@Test
	void testFindAllTarefas() throws Exception {
		String url = String.format("/api/v1/lista-tarefa/%d/tarefas", this.listaTarefaSalva.getId());
		mockMvc.perform(get(url))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());

	}

	@Test
	void testFindAllListaTarefaById() throws Exception {
		Tarefa tarefa = new Tarefa();
		tarefa.setDescricao("Minha Tarefa");
		tarefa.setListaTarefa(this.listaTarefaSalva);

		Tarefa tarefaSalva = this.tarefaService.save(tarefa);

		Long idTarefa = tarefaSalva.getId();

		this.mockMvc.perform(get("/api/v1/tarefa/" + idTarefa))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id", is(tarefaSalva.getId()), Long.class))
				.andExpect(jsonPath("$.descricao", is(tarefaSalva.getDescricao())))
				.andExpect(jsonPath("$.idListaTarefa", is(tarefaSalva.getListaTarefa().getId()), Long.class));

	}

	@Test
	void testSaveListaTarefas() throws JsonProcessingException, Exception {
		Tarefa tarefa = new Tarefa();
		tarefa.setDescricao("Minha Tarefa");
		tarefa.setListaTarefa(this.listaTarefaSalva);

		TarefaSaveDTO tarefaSaveDTO = TarefaSaveMapper.INSTANCE.toDto(tarefa);

		this.mockMvc.perform(post("/api/v1/tarefa/")
				.content(this.objectMapper.writeValueAsString(tarefaSaveDTO))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id", isA(Integer.class)))
				.andExpect(jsonPath("$.descricao", is(tarefaSaveDTO.getDescricao())))
				.andExpect(jsonPath("$.idListaTarefa", is(tarefaSaveDTO.getIdListaTarefa()), Long.class));
	}

	@Test
	void testUpdateListaTarefas() throws JsonProcessingException, Exception {
		Tarefa tarefa = new Tarefa();
		tarefa.setDescricao("Minha Tarefa");
		tarefa.setListaTarefa(this.listaTarefaSalva);

		Tarefa tarefaSalva = this.tarefaService.save(tarefa);

		tarefaSalva.setDescricao("Nome Diferente");

		TarefaUpdateDTO tarefaUpdateDTO = TarefaUpdateMapper.INSTANCE.toDto(tarefaSalva);

		this.mockMvc.perform(put("/api/v1/tarefa")
				.content(this.objectMapper.writeValueAsString(tarefaUpdateDTO))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id", is(tarefaUpdateDTO.getId()), Long.class))
				.andExpect(jsonPath("$.descricao", is(tarefaUpdateDTO.getDescricao())))
				.andExpect(jsonPath("$.idListaTarefa", is(tarefaSalva.getListaTarefa().getId()), Long.class));
	}

	@Test
	void testDeleteListaTarefaById() throws JsonProcessingException, Exception {
		Tarefa tarefa = new Tarefa();
		tarefa.setDescricao("Minha Tarefa");
		tarefa.setListaTarefa(this.listaTarefaSalva);

		Tarefa tarefaSalva = this.tarefaService.save(tarefa);

		this.mockMvc.perform(delete("/api/v1/tarefa/" + tarefaSalva.getId()))
				.andExpect(status().isNoContent());
		assertThrows(EntityNotFoundException.class, () -> {
			this.tarefaService.throwErrorIfNotExistsById(tarefaSalva.getId());
		});

	}

}
