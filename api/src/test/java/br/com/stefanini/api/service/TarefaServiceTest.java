package br.com.stefanini.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.Tarefa;

@SpringBootTest
@ActiveProfiles("dev-h2")
class TarefaServiceTest {

	@Autowired
	private ListaTarefaService listaTarefaService;

	@Autowired
	private TarefaService tarefaService;

	private Tarefa tarefa;
	private Tarefa tarefa2;

	private ListaTarefa listaTarefa;

	@BeforeEach
	void setUp() throws Exception {
		tarefa = new Tarefa();
		tarefa2 = new Tarefa();

		listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha lista de tarefas");

		ListaTarefa listaSalva = listaTarefaService.save(listaTarefa);

		tarefa.setListaTarefa(listaSalva);
		tarefa.setDescricao("HomeWork");

		tarefa2.setListaTarefa(listaSalva);
		tarefa2.setDescricao("Escrever um poema");

	}

	@AfterEach
	void tearDown() throws Exception {
		this.listaTarefaService.deleteAll();

	}

	@Test
	void testDelete() {
		Tarefa tarefaSalvo = this.tarefaService.save(tarefa);

		this.tarefaService.delete(tarefaSalvo.getId());

		assertThrows(EntityNotFoundException.class, () -> {
			this.tarefaService.findById(tarefaSalvo.getId());
		});

	}

	@Test
	void testFindAll() {
		this.tarefaService.save(tarefa);
		this.tarefaService.save(tarefa2);

		List<Tarefa> tarefas = this.tarefaService.findAll();

		assertEquals(2, tarefas.size());
		assertEquals(tarefa.getDescricao(), tarefas.get(0).getDescricao());

	}

	@Test
	void testFindById() {

		Tarefa expectedUser = this.tarefaService.save(tarefa);

		Long expectedId = expectedUser.getId();

		Tarefa actualUser = this.tarefaService.findById(expectedId);

		assertEquals(expectedUser.getDescricao(), actualUser.getDescricao());

	}

	@Test
	void testSave() {
		Tarefa tarefaSalvo = this.tarefaService.save(tarefa);

		assertEquals(tarefaSalvo.getDescricao(), tarefa.getDescricao());

	}

	@Test
	void testThrowErrorIfNotExistsById() {
		assertThrows(EntityNotFoundException.class, () -> {
			this.tarefaService.throwErrorIfNotExistsById(200L);
		});
	}

	@Test
	void testUpdate() {
		Tarefa tarefaAlterar = this.tarefaService.save(tarefa);
		tarefaAlterar.setDescricao("Novo nome");

		Tarefa tarefaAlterado = this.tarefaService.update(tarefaAlterar);

		assertEquals(tarefaAlterar.getDescricao(), tarefaAlterado.getDescricao());

	}

}
