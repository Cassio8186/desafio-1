package br.com.stefanini.api.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.stefanini.api.exceptions.EntityNotFoundException;
import br.com.stefanini.api.model.ListaTarefa;
import br.com.stefanini.api.model.dto.ListaTarefaSaveDTO;
import br.com.stefanini.api.model.dto.ListaTarefaUpdateDTO;
import br.com.stefanini.api.model.dto.mapper.ListaTarefaSaveMapper;
import br.com.stefanini.api.model.dto.mapper.ListaTarefaUpdateMapper;
import br.com.stefanini.api.service.ListaTarefaService;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("dev-h2")
class ListaTarefaControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ListaTarefaService listaTarefaService;

	@AfterEach
	public void tearDown() {
		this.listaTarefaService.deleteAll();
	}

	@Test
	void testFindAllListasTarefas() throws Exception {
		mockMvc.perform(get("/api/v1/lista-tarefa"))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());

	}

	@Test
	void testFindAllListaTarefaById() throws Exception {
		ListaTarefa listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha Lista");

		ListaTarefa listaTarefaSalva = this.listaTarefaService.save(listaTarefa);

		Long idTarefa = listaTarefaSalva.getId();

		this.mockMvc.perform(get("/api/v1/lista-tarefa/" + idTarefa))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id", is(listaTarefa.getId()), Long.class))
				.andExpect(jsonPath("$.nome", is(listaTarefa.getNome())));

	}

	@Test
	void testSaveListaTarefas() throws JsonProcessingException, Exception {
		ListaTarefa listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha Lista");

		ListaTarefaSaveDTO listaTarefaSaveDTO = ListaTarefaSaveMapper.INSTANCE.toDto(listaTarefa);

		this.mockMvc.perform(post("/api/v1/lista-tarefa/")
				.content(this.objectMapper.writeValueAsString(listaTarefaSaveDTO))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id", isA(Integer.class)))
				.andExpect(jsonPath("$.nome", is(listaTarefaSaveDTO.getNome())));
	}

	@Test
	void testUpdateListaTarefas() throws JsonProcessingException, Exception {
		ListaTarefa listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha Lista");

		ListaTarefa listaTarefaSalva = this.listaTarefaService.save(listaTarefa);

		listaTarefaSalva.setNome("Nome Diferente");

		ListaTarefaUpdateDTO listaTarefaUpdateDTO = ListaTarefaUpdateMapper.INSTANCE.toDto(listaTarefaSalva);

		this.mockMvc.perform(put("/api/v1/lista-tarefa/")
				.content(this.objectMapper.writeValueAsString(listaTarefaUpdateDTO))
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id", is(listaTarefaUpdateDTO.getId()), Long.class))
				.andExpect(jsonPath("$.nome", is(listaTarefaUpdateDTO.getNome())));
	}

	@Test
	void testDeleteListaTarefaById() throws JsonProcessingException, Exception {
		ListaTarefa listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha Lista");

		ListaTarefa listaTarefaSalva = this.listaTarefaService.save(listaTarefa);

		this.mockMvc.perform(delete("/api/v1/lista-tarefa/" + listaTarefaSalva.getId()))
				.andExpect(status().isNoContent());
		assertThrows(EntityNotFoundException.class, () -> {
			this.listaTarefaService.throwErrorIfNotExistsById(listaTarefaSalva.getId());
		});

	}

	@Test
	void testDeleteAllListaTarefas() throws Exception {
		ListaTarefa listaTarefa = new ListaTarefa();
		listaTarefa.setNome("Minha Lista");
		ListaTarefa ListaSalva = this.listaTarefaService.save(listaTarefa);
		listaTarefa.setNome("Lista");
		ListaTarefa ListaSalva2 = this.listaTarefaService.save(listaTarefa);

		this.mockMvc.perform(delete("/api/v1/lista-tarefa/"))
				.andExpect(status().isNoContent());

		assertThrows(EntityNotFoundException.class, () -> {
			this.listaTarefaService.throwErrorIfNotExistsById(ListaSalva.getId());
			this.listaTarefaService.throwErrorIfNotExistsById(ListaSalva2.getId());
		});
	}

}
