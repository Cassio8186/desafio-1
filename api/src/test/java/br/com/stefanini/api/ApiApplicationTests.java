package br.com.stefanini.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("dev-h2")
class ApiApplicationTests {

	@Test
	void contextLoads() {
	}

}
